<?php
namespace Drupal\uc_edit_order_items\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\field\FieldConfigInterface;
use Drupal\user\Entity;
use Drupal\uc_order\Entity\Order;
use Drupal\uc_order\OrderInterface;



/**
 * Configure example settings for this site.
 */
class product_attribute_edit_form extends FormBase {
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'uc_edit_order_items';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'uc_edit_order_items',
    ];
  }




  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $opid = 0) {

 
  //get the order product data
    $data = uc_edit_order_items_get_item_data($opid);
    $cart = uc_edit_order_items_get_product_data($data['nid']);



  $headers = array('Attribute');


  $info = "<p><b>WARNING:</b> Expert use only. The changes made here will be saved directly to the database. No change in calculated
  amounts will be made, and there is no checking of permissible values. This is experimental.</p>";

  $form['info'] = array(
    '#type' => 'item',
    '#markup' => $info,
  );

    $form['opid'] = array(
    '#type' => 'hidden',
    '#value' => $opid,
  );

      $form['oid'] = array(
    '#type' => 'hidden',
    '#value' => $data['order_id'],
  );

      $form['rawdata'] = array(
    '#type' => 'hidden',
    '#value' => $data['rawdata'],
  );


  $form['atr'] = array(

    '#type' => 'table',
    '#id' => 'attTable',
    '#caption' => $this->t('See and Review Product Attributes in this order'),
    '#header' => $headers,
   );


    
  foreach($data['data']['attributes'] as $label=>$attribute){


    if(isset($cart) && isset($cart[$label] )){

      
    $form['atr'][$label] = array(
 
      'sel' => array(
        '#type' => 'select',
        '#title' => $label,
        '#options' => $cart[$label]['options'],
        '#default_value' => key($attribute),
      ),

      'options' => array(
        '#type' => 'hidden',
        '#value' => serialize($cart[$label]['options']),
      ),  
    );


    } else{

    $form['atr'][$label] = array(
      0 => array(
        '#type' => 'textfield',
        '#title' => $label,
        '#default_value' => implode('',$attribute),
      ),
  
    );


    }



    

  }
      // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#description' => $this->t('Submit, #type = submit'),
    ];


    return $form;
  }


/**
 * {@inheritdoc}
 */
public function validateForm(array &$form, FormStateInterface $form_state) {
  //dsm($form_state);
}

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration


//\Drupal::service('entity_type.manager')->getStorage('commerce_order')->resetCache([ORDER_ID]);
    
    $vals = $form_state->getValues();
    
    $opid = $vals['opid']; // the order product id
    $oid = $vals['oid']; // the order id
    $order = Order::load($oid);

    $product = $order->products[$opid];

    $prodatt = $product->data->attributes;

    $formatt = $vals['atr'];


    //process the selected attributes
    foreach($formatt as $key=>&$attr){
      if(isset($attr['sel'])){
        $list = unserialize($attr['options']);
        $attr = array($attr['sel']=>$list[$attr['sel']]); 
        $formatt[$key] = $attr;
        unset($attr['options']);
      }
      $product->data->attributes[$key]=$attr;
    }

    uc_order_product_save($oid, $product);
    $order->save();


    $rawdata = $vals['rawdata'];
    $oid = $vals['oid'];
 
    $indata = unserialize($rawdata);
    $indata['attributes'] = $formatt;
    $outdata = serialize($indata);

    $sql = "UPDATE {uc_order_products} SET `data` = :newdat WHERE order_product_id = :opid";
    $result = \Drupal::database()->query($sql, array(':newdat'=>$outdata, ':opid' => $opid));

   
    
drupal_set_message("Resetting order number $oid");
    \Drupal::entityManager()->getStorage('uc_order')->resetCache([$oid]);
  
  }
}